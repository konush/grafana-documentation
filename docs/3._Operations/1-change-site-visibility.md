# Change visibility of the Grafana instance

To change the Visibility of the Grafana instance in order to allow users to access the instance from inside/outside CERN, there are 2 ways of doing it:

One is to set `Visibility` to `Internet` at the time of creating the Grafana instance (see [2-create-grafana-instance](../1._Installation/2-create-grafana-instance.md) for further info):

![image-20.png](../assets/image-20.png)

Another is to edit the `Visibility` value once the Grafana instance is already running. To do so, select your project under <https://app-catalogue.cern.ch> (or <https://app-cat-stg.cern.ch> for development purposes), change to the `Administrator` environment, and under `Operators`, click on `Installed Operators`, then click on the Grafana tab:

![image-21.png](../assets/image-21.png)

Click under your Grafana instance, go to the YAML tab, and type `Internet` or `Intranet` where the `Visibility` field is located. Note that the value is case sensitive, so any other value different than Internet or Intranet will fail.

![image-22.png](../assets/image-22.png)

Finally, <kbd>Save</kbd> and your instance will be accessible from inside and/or outside CERN immediately.

# Configure CERN SSO

When a user creates a new project, OKD4 automatically creates an application registration in the new [Application Portal](https://application-portal.web.cern.ch) for the OKD4 project. On the portal, the application registration will be named according to the naming schema `webframeworks-app-catalogue-PROJECT_NAME` (`webframeworks-app-cat-stg-PROJECT_NAME` for staging environment).

As mentioned before, no local `admin` account is created. Thus, once we deploy our Grafana instance, we just have `Viewer` access to our Grafana instance.

To add new `administrators`, `editors`, and `viewers`, we need to create specific roles under the application registration associated to our project.

To do so, navigate to <https://application-portal.web.cern.ch> and look for your application registration, and go to the tab `Roles`:

![image-4.png](../assets/image-4.png)

Now, let's add new roles. We need to add at least 3 roles, one for `administrators`, another for `editors` and for `viewers`.

In order to create these roles, **it is important to specify the proper** `Role Identifier`. That means:

- For administrators will be `grafana-administrators`.
- For editors will be `grafana-editors`.
- For viewers will be `grafana-viewers`.

!!! warning
    Note that, by default, any other role specified will acquire the `Viewer` role under the Grafana instance. This also applies to the `Default role` that comes by default with the application registration. In order to avoid this behavior, you just need to delete the Default role under your application registration, thus preventing any logged user to access your Grafana instance.

When configuring the different roles, do not forget to accommodate the `Minimum Level Of Assurance` at your best convenience.

![image-5.png](../assets/image-5.png)

The final result will look like:

![image-6.png](../assets/image-6.png)

Finally, we just need to **Assign roles to groups**:

![image-7.png](../assets/image-7.png)

We look for our preferred group. In the case that we do not have a existing group, we can always create one under <https://groups-portal.web.cern.ch/group/create>:

![image-8.png](../assets/image-8.png)

And that's it. People belonging to that group(s) will acquire the aforementioned role, and they can login into the Grafana instance with their CERN account (by acquiring their corresponding role).

!!! info
    It might happens that the owner of the Grafana instance signs in as `Viewer` for first time, and after configuring the different roles under the applications portal, specially the `Administrator` role, it still has the same `Viewer` role. For an appropriate grantion of the roles, consider login out from the Grafana instance, and then re-login. If persisting, try to open an incognito window and sign in again. This can be also solved by cleaning cookies in your browser.

# Create a Grafana instance

Under the `Administrator` environment, go to `Operator` > `Installed Operators` and click on the `Grafana Operator`:

![image-1.png](../assets/image-1.png)

Now, let's create a new instance of Grafana.

![image-2.png](../assets/image-2.png)

!!! info
    By default, Grafana instances will be created with CERN SSO ready-to-use, and no local `admin` user will be created. This can be obviously overridden at any point.

First, set an appropriate `Name` for your Grafana instance. This will help users to identify the Grafana instance under a specific project.

Then, set the `Hostname`, i.e., the url where to access the Grafana instance to (e.g., `my-grafana.web.cern.ch`, or `my-awesome-grafana.app.cern.ch`). In case you are using <https://app-cat-stg.cern.ch>, consider using the `webtest.cern.ch` subdomain for the hostname of your Grafana instances.

In case you need some extra configuration for Grafana to run, like an external database, expand the `Config` element to do so. Check <https://grafana.com/docs/grafana/latest/administration/configuration/> for information about the different elements offered.

![image-3.png](../assets/image-3.png)

If you need any extra plugin for your Grafana instance, you can do it either from the Grafana UI, or by using the `Install Plugins` element.

Finally, configure the `Visibility` of the Grafana instance accordingly. This will determine whether your instance is accessible from inside or outside CERN.

Click on <kbd>Create</kbd> to launch the creation. 

To check whether our instance is ready to use, we can check the status of the installation:

![image-17.png](../assets/image-17.png)

And see the conditions of the installation:

![image-18.png](../assets/image-18.png)

Last but not least, a good indicator that everything is ready it is to visit the pod and check for the logs.

![image-19.png](../assets/image-19.png)

# Delete a project

In order to delete a project, and as temporary solution, we need to go to the proper cluster <https://app-catalogue.cern.ch> (<https://app-cat-stg.cern.ch> for staging applications).

Then, click the _kebab_ button and finally click on `Delete Project`.

![image-29.png](../assets/image-29.png)

It's done. Your project will eventually be deleted in the coming seconds.

!!! info
    Be sure that indeed, you want to delete the project. This is a **destructive operation** and possibilities of recovering things are practically nonexistent.

# Create a project

To create a project in order to instantiate a new Grafana instance, first we need to create a project under <https://app-catalogue.cern.ch>.

For development purposes, consider using <https://app-cat-stg.cern.ch>.

![image.png](../assets/image.png)

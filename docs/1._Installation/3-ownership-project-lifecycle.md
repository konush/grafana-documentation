# Ownership and project lifecycle

For the Ownership and project lifecycle documentation, refer to the [PaaS documentation](https://paas.docs.cern.ch/1._Getting_Started/3-ownership-lifecycle/) link.
